<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 02.05.2019
 * Time: 15:22
 */

namespace App\Controllers;


use App\Controller;
use App\View;

class Article extends Controller
{
    //проверка доступа для пользователя
//    protected function access(): bool
//    {
//        return isset($_GET['name']) && 'Vasya' == $_GET['name'];
//    }

    protected function handle()
    {
        $this->view->article = \App\Models\Article::findById($_GET['id']);
        echo $this->view->render(__DIR__ . '/../../templates/article.php');
    }
}