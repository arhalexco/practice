<?php
//[Строка подключения]

try {
    $dbh = new \PDO('mysql:host=localhost;dbname=pdo_tutorial', 'root', 'pass');
} catch (PDOException $e){
    exit($e->getMessage());
}

//[Queries]

//Создание таблицы

const SQL_CREATE_MENU_TABLE = '
     CREATE TABLE IF NOT EXIST menu(
       id INT UNSIGNED AUTO_INCREMENT NOT NULL,
       parent_id INT UNSIGNED,
       title VARCHAR(255) NOT NULL,
       url VARCHAR(500) NOT NULL,
       PRIMARY KEY (id)
     )
';

//Вставка элемента

const SQL_INSERT_MENU_ITEM = '
     INSERT INTO menu (parent_id, title, url) VALUES (?, ?, ?)
';

//Выполнение запроса с неименованными параметрами

$stmt = $pdo->prepare(SQL_INSERT_MENU_ITEM);
$res = $stmt->execute([null, 'Post #1', '/post/1']);


//Изменение элемента

const SQL_UPDATE_MENU_ITEM_BY_ID = '
     UPDATE menu SET
       parent_id = :parent_id,
       title = :title,
       url = :url
     WHERE
       id = :id
';

//Выполнение запроса с именованными параметрами

$stmt = $pdo->prepare(SQL_UPDATE_MENU_ITEM_BY_ID);
$res = $stmt->execute([
    ':parent_id' => null,
    ':title' => 'Post #1',
    ':url' => '/post/1',
    ':id' => 1,
]);




//Вывод всех элементов

const SQL_GET_MENU = '
     SELECT id, parent_id, title, url FROM menu
';

//Вывод одного по id

const SQL_GET_MENU_ITEM = '
     SELECT id, parent_id, title, url FROM menu WHERE id = :id
';

const SQL_DELETE_MENU_ITEM ='
    DELETE FROM menu WHERE id = :id
';