<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 09.04.2019
 * Time: 16:28
 */

namespace App;

use App\Db;

abstract class Model
{
    public const TABLE = '';

    public $id;

    abstract public function getModelName();

    public static function findAll()
    {
        $db = new Db();
        $sql = 'SELECT * FROM ' . static::TABLE;
        return $db->query(
            $sql,
            static::class,
            []
        );
    }

    public static function findById($id)
    {
        $db =new Db();

        $query = 'SELECT * FROM `' . static::TABLE . '` WHERE `id` = :id';
        $data = $db->query(
            $query,
            static::class,
            [':id' => (int)$id]
        );
        return $data ? $data[0] : null;
    }

}