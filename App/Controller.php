<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 02.05.2019
 * Time: 15:24
 */

namespace App;


abstract class Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = new View();
    }

    protected function access(): bool
    {
        return true;
    }

    public function __invoke()
    {
        if ($this->access()){
            $this->handle();
        } else {
            die('Not access');
        }
    }

    abstract protected function handle();

}