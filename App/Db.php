<?php

namespace App;

class Db
{
    protected $dbh;


    public function __construct()
    {
        $config = (include __DIR__ . '/config.php')['db'];
        $this->dbh = new \PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'],
            $config['user'],
            $config['password']
        );
    }

    public function query($sql, $class, $params = [])
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);

        return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
    }

    public function insert($query, $data=[])
    {
        $sth = $this->dbh->prepare($query);
        return $sth->execute($data);

    }

    public function update($query, $params)
    {
        $sth = $this->dbh->prepare($query);
        if ($sth->execute($params))
            return 'TRUE';

    }

    public function getLastId()
    {
        return $this->dbh->lastInsertId();//Узнать номер последней вставленной записи
    }

}