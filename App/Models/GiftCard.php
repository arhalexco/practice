<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 17.04.2019
 * Time: 14:51
 */

namespace App\Models;


use App\Model;

class GiftCard extends Model implements Orderable
{
    public const TABLE = 'cards';

    public function getModelName()
    {
        return 'Покупка';
    }


    public function getTitle()
    {
        return 'Title';
    }

    public function getPrice()
    {
        return 42;
    }

    public function getWeight()
    {
        return 24;
    }
}