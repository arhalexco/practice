<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 17.04.2019
 * Time: 15:02
 */

namespace App\Models;


interface HasWeight
{
    public function getWeight();
}