<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 17.04.2019
 * Time: 15:00
 */

namespace App\Models;


interface HasPrice
{
    public function getPrice();

}