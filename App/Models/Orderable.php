<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 17.04.2019
 * Time: 14:46
 */

namespace App\Models;


interface Orderable extends HasPrice, HasWeight
{
    public function getTitle();
}