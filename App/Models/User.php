<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 09.04.2019
 * Time: 16:11
 */

namespace App\Models;


use App\Model;

class User extends Model
{
    public const TABLE = 'users';

    public $email;
    public $name;

    public function getModelName()
    {
        return 'Пользователь';
    }
}