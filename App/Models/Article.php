<?php
/**
 * Created by PhpStorm.
 * User: mr.alex
 * Date: 09.04.2019
 * Time: 15:32
 */

namespace App\Models;


use App\Model;
use App\Db;

class Article extends Model
{
    public const TABLE = 'news';

    public $title;
    public $content;


    public function getModelName()
    {
        return 'Новость';
    }

    public function insertArticle()
    {
        $db = new Db();

        $fields = get_object_vars($this);
        $cols = [];
        $data = [];

        foreach ($fields as $name => $value){
            if ('id' == $name){
                continue;
            }
            $cols[] = $name;
            $data[':' . $name] = $value;
        }
        $query = 'INSERT INTO ' . static::TABLE . ' (' . implode(',', $cols) . ') VALUES (' . implode(',', array_keys($data)) . ')';
        $db->insert($query, $data);

        $this->id = $db->getLastId();

    }

    public function updateArticle($id)
    {
        $db = new Db();

        $query = 'UPDATE ' . static::TABLE . ' SET title = :title, content = :content WHERE id = :id';
        $params = ['id' => $id, 'title' => $this->title, 'content' => $this->content];
        $db->update($query, $params);
    }

    public function daleteArticle($id)
    {
        $db = new Db();

        $query = 'DELETE FROM ' . static::TABLE . '  WHERE id = :id';
        $params = ['id' => $id];
        $db->update($query, $params);
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function save()
    {
        if($this->isNew()){
            $this->insertArticle();
        } else{
            $this->updateArticle();
        }
    }
}